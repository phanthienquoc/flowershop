module.exports = {
  i18n: {
    defaultLocale: "en",
    locales: ["en", "vi", "de-AT", "de-DE", "de-CH"],
  },
  fallbackLng: {
    default: ["en"],
  },
  nonExplicitSupportedLngs: true,
};
