import styles from './moduleContainer.module.css';

import React from 'react';
import Item from '../item';


const ModuleContainer = (props: any) => {
    const { label = "Title", items = Array(5).fill("") } = props;

    return (
        <div className={styles.moduleContainer}>
            <h3 className={styles.label}>{label}</h3>
            <div className={styles.itemContainer}>
                {items.map(((item: any, index: number) => (<Item key={index} {...item} />)))}
            </div>
        </div>
    )
}


export default ModuleContainer