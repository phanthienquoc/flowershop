import styles from './bestSeller.module.css';

import React from 'react';
import Image from 'next/image';


const BestSellerContainer = (props: any) => {
    const { label = "Title", items = Array(5).fill({
        src: "/assets/images/demo.png",
        label: "best seller"
    }) } = props;

    return (
        <div className={styles.moduleContainer}>
            <h3 className={styles.label}>{label}</h3>
            <div className={styles.itemContainer}>
                <div>
                    <Image alt={items[0].label} src={items[0].src} height={608} width={608} />
                </div>
                <div>
                    <div className={styles.subItemContainer}>
                        {items.slice(1, 5).map(((item: any, index: number) => (<Image src={item.src} height={296} width={296} alt={item.label} key={index} />)))}
                    </div>
                </div>
            </div>
        </div>
    )
}


export default BestSellerContainer