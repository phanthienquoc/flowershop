import React from 'react';
import Image from 'next/image';

import styles from './item.module.css'

const Item = (props: any) => {
    const { src = "/assets/images/demo.png", label = "Title", price = "297" } = props;
    return (
        <div className={styles.item}>
            <Image alt='Mountains' width={296} height={296} src={src} />
            <h5 className={styles.label}>{label}</h5>
            <div className={styles.lastRow}>
                <p className={styles.price}>{price}</p>
                <div className={styles.iconContainer}>

                </div>
            </div>
        </div >
    )
}

export default Item