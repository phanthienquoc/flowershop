import React from 'react';

const ProductDetail = (props: any) => {
    return (
        <div>
            <h1>
                Products Detail {props.id}
            </h1>
            <div>
                <h3>
                    Product Id:  {props.id}
                </h3>
                <h5>
                    Product Item Id {props.id}
                </h5>
            </div>
        </div>
    )
}

export async function getStaticPaths() {
    return {
        paths: [
            // String variant:
            '/product/id',
            // Object variant:
            { params: { id: '1' } },
        ],
        fallback: true,
    }
}

export async function getStaticProps({ params }: any) {
    return {
        props: {
            id: params.id
        },
    }
}

export default ProductDetail