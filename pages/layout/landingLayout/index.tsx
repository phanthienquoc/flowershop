import styles from './landingLayout.module.css';

import React from 'react';
import Image from 'next/image';
import BannerImage from '../../../public/assets/images/landingPage/header/header.png';

const LandingLayout = ({ children }: any) => {
    return (
        <div className={styles.landingLayout}>
            <div className={styles.landingBanner}>
                <Image style={{ width: "100%" }} alt="Picture of the author" src={BannerImage} />
            </div>
            <div className={styles.landingContent}>
                {children}
            </div>
        </div>
    )
}

export default LandingLayout;