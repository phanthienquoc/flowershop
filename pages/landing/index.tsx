import React, { ReactElement } from 'react';
import LandingLayout from '../layout/landingLayout';
import ModuleContainer from '../shareComponents/moduleContainer';
import BestSellerContainer from '../shareComponents/bestSeller';


const Landing = (props: any) => {
    return (
        <>
            <div style={{ marginTop: "4rem" }}>
                <BestSellerContainer label={"Best Seller"} />
            </div>
            <div style={{ marginTop: "4rem" }}>
                <ModuleContainer label={"Wedding & Events"} />
            </div>
            <div style={{ marginTop: "4rem" }}>
                <ModuleContainer label={"Gift Sets"} />
            </div>
        </>
    )
}

Landing.getLayout = function getLayout(page: ReactElement) {
    return (
        <LandingLayout>
            {page}
        </LandingLayout>
    )
}


export default Landing;